from hanoi_vision import Visualisation_hanoi

def bouge(x,y):
    # vérification déplacement licite
    if len(hanoi[x])==0: # La tour de départ est vide
        print("pas de pion à déplacer")
    elif len(hanoi[y])==0: # la tour de départ est pleine et la tour de destination est vide
        pion=hanoi[x].pop()
        hanoi[y].append(pion)
    elif hanoi[x][-1]<hanoi[y][-1]: # Le pion sur la tour de départ est il inferieur au pion sur la tout d'arrivée
        pion=hanoi[x].pop()
        hanoi[y].append(pion)
    else: # pion de départ est superieur à pion arrivée
        print("Déplacement illicite")
    print(hanoi)
    v.mise_a_jour(hanoi)
    

def deplace2pîons(depart,arrivee,intermediaire):
    bouge(depart,intermediaire)
    bouge(depart,arrivee)
    bouge(intermediaire,arrivee)

def deplaceNpions(n,depart,arrivee,intermediaire):
    if n==2:
        deplace2pîons(depart,arrivee,intermediaire)
    else:
        deplaceNpions(n-1, depart,intermediaire,arrivee)
        bouge(depart,arrivee)
        deplaceNpions(n-1,intermediaire,arrivee,depart)

tour0=[5,4,3,2,1]
tour1=[]
tour2=[]
hanoi=[tour0,tour1,tour2]
print(hanoi)
v=Visualisation_hanoi(hanoi)

deplaceNpions(5,0,2,1)


